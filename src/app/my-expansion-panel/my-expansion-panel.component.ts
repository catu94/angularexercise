import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'my-expansion-panel',
  templateUrl: './my-expansion-panel.component.html',
  styleUrls: ['./my-expansion-panel.component.scss'],
  animations: [
    trigger('expandCollapse', [
      state('collapsed', style({
        height: '0',
      })),
      state('expanded', style({
        height: '*',
      })),
      transition('collapsed <=> expanded', [
        animate('0.5s')
      ]),
    ]),
  ]
})
export class MyExpansionPanelComponent implements OnInit {

  state: string;

  constructor() {
    this.state = 'collapsed';
  }

  ngOnInit() {
  }

  toggle() {
    this.state = (this.state === 'collapsed') ? 'expanded' : 'collapsed';
  }

}
